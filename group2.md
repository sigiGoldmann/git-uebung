    $ git checkout master     switch to 'master' branch
    $ git branch feature1     create branch 'feature1'
    $ git checkout -b work    switch to 'work' branch and create it, if it does not exist
    $ git checkout work       switch to 'work' branch
    $ git branch              list all local branches
    $ git branch -v           list all local branches including Hash and message of last commit
    $ git branch -a           list all local and remote branches
    $ git branch --merged     list branches merged into HEAD
    $ git branch --no-merged  list branches that have not merged
    $ git branch -d work      delete branch 'work' when already merged
    $ git branch -D work      delete brnach 'work', forcing deletion
