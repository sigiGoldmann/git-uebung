# git cheat sheet

Read your group instruction in the text files 

[group1.md](group1.md)

[group2.md](group2.md)

[group3.md](group3.md)

[group4.md](group4.md)

and place your solution in your corresponding text file. Later, we will integrate all solutions into this file.

## Creating repositories

    $ git init                    Create a new local git repository
    $ git clone https://...       Clone a Remote Repository

## Staging and committing

    $ git add test.txt            The file test.txt file is added to the stage
    $ git reset HEAD test.txt     Reverting to the state of the HEAD
    $ git add .                   All edited files are added to the stage except deleted files
    $ git add -u                  Only modified files where staged
    $ git add -A                  Everything will be staged
    $ git diff --staged           Showing the changes between the staged files and the current HEAD
    $ git commit                  Commit staged files. A commit message has to be specified afterwards
    $ git commit -a               Only modified files where staged and commited
    $ git commit -m "My message"  Commit staged files using the given commit message

## Inspecting the repository and history

    $ git diff                  Zeigt die Unterschiede zwischen dem Arbeitsverzeichnis und der Stage
    $ git diff --staged         Zeigt die Unterschiede zwischen der Stage und dem letzten comitteten Zustand
    $ git diff test.txt         Zeigt die Unterschiede der Datei test.txt zwischen Arbeitsverzeichnis und der Stage
    $ git diff --theirs         Zeigt bei einem Merge-Konflikt die konflikterzeugenden Äderungen auf dem Zielbranch
    $ git diff --ours           Zeigt bei einem Merge-Konflikt die konflikterzeugenden Änderungen auf dem aktuellen Branch
    $ git log                   Zeigt die Commit-Historie
    $ git log --oneline         Zeigt die Commit-Historie: ein Commit - eine Zeile
    $ git log --oneline --all   Zeigt die Commit-Historie: ein Commit - eine Zeile, inklusive der 'toten' commits
    $ git log --oneline --all --graph Zeigt die Commit-Historie in einer Baumstruktur: ein Commit - eine Zeile, inklusive der 'toten' commits
    $ git log --follow -p -- filename Zeigt die Commit-Historie der Datei filename in Form von Patches, Umbenennungen werden berücksichtigt
    $ git log -S'static void Main'    Zeigt alle Commits, die 'static void Main' im Commit-Kommentar enthalten
    $ git log --pretty=format:"%h - %an, %ar : %s"  Zeigt die Commit-Historie im Format: Hash - Autor, relative Zeitangabe des Commits : Commit-Kommentar 


## Managing branches

    $ git checkout master     switch to 'master' branch
    $ git branch feature1     create branch 'feature1'
    $ git checkout -b work    switch to 'work' branch and create it, if it does not exist
    $ git checkout work       switch to 'work' branch
    $ git branch              list all local branches
    $ git branch -v           list all local branches including Hash and message of last commit
    $ git branch -a           list all local and remote branches
    $ git branch --merged     list branches merged into HEAD
    $ git branch --no-merged  list branches that have not merged
    $ git branch -d work      delete branch 'work' when already merged
    $ git branch -D work      delete brnach 'work', forcing deletion

## Merging

Explain the following commands with one sentence:

    $ git checkout work             Mit diesem Befehl wechselt man auf den Branch mit Namen 'work'.
    $ git checkout master           Mit diesem Befehl wechselt man von einem Branch auf den master-Branch.
    $ git merge work                Mit diesem Befehl wird der Merge Prozess vom Branch 'work' in den aktuell ausgecheckten Branch gestartet.
    $ git merge --abort             Bei Auftreten eines Merge-Konfliktes kann hiermit der merge-Prozess abgebrochen werden.
    $ git cat-file -p a3798b        Befindet man sich im .git/objects/{xy} Ordner (wobei {xy} für die ersten beiden Zeichen eines Fingerprints
                                    (git-Hash-Code) steht), wird mit diesem Befehl der Inhalt des Git-Objectes mit diesem Fingerprint angezeigt. Dies kann der Inhalt einer Datei oder beispielsweise der Inhalt eines Commits sein.

Example:

    $ git status         Shows the current state of the repository


Here are some standard situations, and a git command. Draw the result, and add some explanation:

Example 1:

    A - B (master)
         \
          C (work)

    $ git checkout master
    $ git merge work

Result and explanation here:
  Mit dieser Kombination von Befehlen wird der 'work' Branch in den master-branch gemerged. Entspricht einem git push bzw. fast-forward, da sich der master-branch noch im gleichen Zustand befindet, wie zum Zeitpunkt der Branch-Erstellung.
  Ergebnis:

    A - B - C(master/work)

Example 2:

    A - B - C - D (master)
         \
          X - Y (work)

    $ git checkout master
    $ git merge work

Result and explanation here:
  Mit dieser Kombination von Befehlen wird wieder der 'work' Branch in den master-branch gemerged. Diesmal wurden aber in beiden Branches commits eingefügt. Im Rahmen des merges wird ein merge-Commit angelegt. ggf. müssen Konflikte gelöst werden.
  Ergebnis:

    A - B - C - D - E (master)
         \         /
           X - Y (work)


Example 3:

    A - B - C - D (master)
         \
          X - Y (work)

    $ git checkout work
    $ git merge master
    $ git checkout master
    $ git merge work

Result and explanation here:
  Mit dieser Kombination von Befehlen wird wieder der 'work' Branch in den master-branch gemerged. Dabei wird aber im ersten Teil die Arbeit auf dem master-branch dem work-Branch hizugefügt. Dadurch wird die Situation aus example1 hergestellt.ggf. auftretende Konflikte werden beim merge des masters in den work-Branch gelöst.
  Ergebnis:

    Schritt 1
    A - B - C - D (master)
          \       \
            X - Y - Z (work)

    Schritt 2
    A - B - C - D
          \       \
            X - Y - Z (master / work)